﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace GenerateCodes
{
    internal class Program
    {
        private static void Main(string[] args)
        {

            string filename = Config.excelpath;
            string filenamewrite = Config.outpath;

            write(filenamewrite, read(filename));
            Console.WriteLine(" All Done !");
            Console.ReadKey();

        }


        private static void write(string fileName, List<Dictionary<string, string>> output)
        {
            var writer = new StreamWriter(File.Open(fileName, FileMode.OpenOrCreate, FileAccess.Write));
            int i = 0;
            foreach (Dictionary<string, string> dictionary in output)
            {
                i++;
                string readystring = dictionary["Template"];
                string prodname = " 商品： "+ dictionary["[Column0]"] + "   品牌： " +dictionary["[Column1]"] +" 代码如下：";

                readystring = Regex.Replace(readystring, @"\t|\n|\r", "");
                readystring = Regex.Replace(readystring, @"\""", "\"");
                writer.WriteLine(prodname);
                writer.WriteLine("\n");
                writer.WriteLine(readystring);
                writer.WriteLine("\n");
                writer.WriteLine("\n");
                writer.WriteLine("==================================");
                Console.WriteLine(i + ". processing "+ prodname + " finished !");
            }



            writer.Close();
            Console.WriteLine("\n"); //outputoutput
        }

        private static List<Dictionary<string, string>> read(string fileName)
        {
            string strConn = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + fileName +
                             ";Extended Properties=\"Excel 12.0;HDR=No;IMEX=1\";";

            using (OleDbConnection connection = new OleDbConnection(strConn))
            {
                List<Dictionary<string, string>> output = new List<Dictionary<string, string>>();
                connection.Open();
                OleDbCommand command = new OleDbCommand("select * from [Sheet1$]", connection);
                using (OleDbDataReader dr = command.ExecuteReader())
                {

                    DataTable sheetColumns = connection.GetOleDbSchemaTable(OleDbSchemaGuid.Columns,
                                                                            new object[] {null, null, "Sheet1$", null});

                    if (sheetColumns != null)
                    {
                        int count = sheetColumns.Rows.Count;

                        while (dr.Read())
                        {
                            var signle = new Dictionary<string, string>();
                            for (int i = 0; i < count; i++)
                            {
                                var rowCol = dr[i].ToString();
                                var utf8 = Encoding.GetEncoding("gb2312");
                                byte[] utfBytes = utf8.GetBytes(rowCol);
                                rowCol = utf8.GetString(utfBytes, 0, utfBytes.Length);
                                signle.Add("[Column" + i + "]", rowCol);
                            }

                            //process template
                            string template = process(signle, getorigintemplate());
                            signle.Add("Template", template);
                            output.Add(signle);
                        }
                    }
                }

                return output;
            }

        }


        private static string process(Dictionary<string, string> data, string template)
        {
            //replace
            foreach (KeyValuePair<string, string> keyValuePair in data)
            {
                if (template.Contains(keyValuePair.Key))
                {
                    template = template.Replace(keyValuePair.Key, keyValuePair.Value);
                }
            }
            return template;
        }

        private static string getorigintemplate()
        {
            string template = @"
<div style=""text-align: left; 685px: ;"">
	<img src=""/fileRepository/product/others/ZIdldCKIQ-m3e4eGrGO-fg.jpg"" /></div>
<div style=""text-align: left; 685px: ;"">
	&nbsp;</div>
<div style=""width: 685px;"">
	<table align=""left"" cellspacing=""2"" class=""table"" style=""border-left: #888 1px solid; border-spacing: 0; border-collapse: collapse; rules: none;"" width=""685"">
		<colgroup>
			<col width=""13%"" />
			<col width=""37%"" />
			<col width=""13%"" />
			<col width=""37%"" />
		</colgroup>
		<tbody>
			<tr>
				<td style=""border-bottom: #ffffff 1px solid; text-align: center; padding-bottom: 5px; background-color: #ec547b; border-top-color: #ffffff; padding-left: 15px; padding-right: 15px; border-left-color: #ffffff; font-weight: bold; border-right: #ffffff 1px solid; padding-top: 5px;"">
					<span style=""font-family: 宋体, tahoma, arial, sans-serif;""><span style=""color: #ffffff; font-weight: bold;"">商品名称</span></span></td>
				<td colspan=""3"" style=""border-bottom: #ffffff 1px solid; text-align: left; padding-bottom: 5px; background-color: #ec547b; border-top-color: #ffffff; padding-left: 15px; padding-right: 15px; border-left-color: #ffffff; font-weight: bold; border-right: #ffffff 1px solid; padding-top: 5px;"">
					<span style=""font-family: 宋体, tahoma, arial, sans-serif;""><span style=""color: #ffffff;"">[Column0]&nbsp; </span></span></td>
			</tr>
			<tr>
				<td style=""border-bottom: #ffffff 1px solid; text-align: center; padding-bottom: 5px; background-color: #fadbd9; border-top-color: #ffffff; padding-left: 15px; padding-right: 15px; vertical-align: baseline; border-left-color: #ffffff; border-right: #ffffff 1px solid; padding-top: 5px;"">
					<span style=""font-family: 宋体, tahoma, arial, sans-serif;""><strong><span style=""color: #ec547b; font-weight: bold;"">品牌</span></strong></span></td>
				<td style=""border-bottom: #ffffff 1px solid; text-align: left; padding-bottom: 5px; background-color: #fadbd9; border-top-color: #ffffff; padding-left: 15px; padding-right: 15px; vertical-align: baseline; border-left-color: #ffffff; border-right: #ffffff 1px solid; padding-top: 5px;"">
					<span style=""font-family: 宋体, tahoma, arial, sans-serif;""><strong><span style=""color: #ec547b;"">[Column1]</span></strong></span></td>
				<td style=""border-bottom: #ffffff 1px solid; text-align: center; padding-bottom: 5px; background-color: #fadbd9; border-top-color: #ffffff; padding-left: 15px; padding-right: 15px; vertical-align: baseline; border-left-color: #ffffff; border-right: #ffffff 1px solid; padding-top: 5px;"">
					<span style=""font-family: 宋体, tahoma, arial, sans-serif;""><strong><span style=""color: #ec547b; font-weight: bold;"">功效</span></strong></span></td>
				<td style=""border-bottom: #ffffff 1px solid; text-align: left; padding-bottom: 5px; background-color: #fadbd9; border-top-color: #ffffff; padding-left: 15px; padding-right: 15px; vertical-align: baseline; border-left-color: #ffffff; border-right: #ffffff 1px solid; padding-top: 5px;"">
					<span style=""font-family: 宋体, tahoma, arial, sans-serif;""><strong><span style=""color: #ec547b;"">[Column2]</span></strong></span></td>
			</tr>
			<tr>
				<td style=""border-bottom: #ffffff 1px solid; text-align: center; padding-bottom: 5px; background-color: #ec547b; border-top-color: #ffffff; padding-left: 15px; padding-right: 15px; border-left-color: #ffffff; border-right: #ffffff 1px solid; padding-top: 5px;"">
					<span style=""font-family: 宋体, tahoma, arial, sans-serif;""><strong><span style=""color: #ffffff; font-weight: bold;"">产品规格</span></strong></span></td>
				<td style=""border-bottom: #ffffff 1px solid; text-align: left; padding-bottom: 5px; background-color: #ec547b; border-top-color: #ffffff; padding-left: 15px; padding-right: 15px; border-left-color: #ffffff; border-right: #ffffff 1px solid; padding-top: 5px;"">
					<span style=""font-family: 宋体, tahoma, arial, sans-serif;""><strong><span style=""color: #ffffff;""><font color=""#ffffff""><font color=""#ffffff""><font color=""#ffffff""><font color=""#ffffff""><font color=""#ffffff"">[Column3]</font></font></font></font></font></span></strong></span></td>
				<td style=""border-bottom: #ffffff 1px solid; text-align: center; padding-bottom: 5px; background-color: #ec547b; border-top-color: #ffffff; padding-left: 15px; padding-right: 15px; border-left-color: #ffffff; border-right: #ffffff 1px solid; padding-top: 5px;"">
					<span style=""font-family: 宋体, tahoma, arial, sans-serif;""><strong><span style=""color: #ffffff; font-weight: bold;"">原产国家</span></strong></span></td>
				<td style=""border-bottom: #ffffff 1px solid; text-align: left; padding-bottom: 5px; background-color: #ec547b; border-top-color: #ffffff; padding-left: 15px; padding-right: 15px; vertical-align: baseline; border-left-color: #ffffff; border-right: #ffffff 1px solid; padding-top: 5px;"">
					<span style=""font-family: 宋体, tahoma, arial, sans-serif;""><strong><span style=""color: #ffffff;"">[Column4]</span></strong></span></td>
			</tr>
			<tr>
				<td style=""border-bottom: #ffffff 1px solid; text-align: center; padding-bottom: 5px; background-color: #fadbd9; border-top-color: #ffffff; padding-left: 15px; padding-right: 15px; border-left-color: #ffffff; font-weight: bold; border-right: #ffffff 1px solid; padding-top: 5px;"">
					<span style=""font-family: 宋体, tahoma, arial, sans-serif;""><span style=""color: #ec547b; font-weight: bold;"">保质期限</span></span></td>
				<td style=""border-bottom: #ffffff 1px solid; text-align: left; padding-bottom: 5px; background-color: #fadbd9; border-top-color: #ffffff; padding-left: 15px; padding-right: 15px; border-left-color: #ffffff; font-weight: bold; border-right: #ffffff 1px solid; padding-top: 5px;"">
					<span style=""font-family: 宋体, tahoma, arial, sans-serif;""><span style=""color: #ec547b;"">[Column5]（具体情况视收到产品为准）</span></span></td>
				<td style=""border-bottom: #ffffff 1px solid; text-align: center; padding-bottom: 5px; background-color: #fadbd9; border-top-color: #ffffff; padding-left: 15px; padding-right: 15px; vertical-align: baseline; border-left-color: #ffffff; border-right: #ffffff 1px solid; padding-top: 5px;"">
					<span style=""font-family: 宋体, tahoma, arial, sans-serif;""><strong><span style=""color: #ec547b; font-weight: bold;"">适用人群</span></strong></span></td>
				<td style=""border-bottom: #ffffff 1px solid; text-align: left; padding-bottom: 5px; background-color: #fadbd9; border-top-color: #ffffff; padding-left: 15px; padding-right: 15px; border-left-color: #ffffff; font-weight: bold; border-right: #ffffff 1px solid; padding-top: 5px;"">
					<span style=""font-family: 宋体, tahoma, arial, sans-serif;""><span style=""color: #ec547b;""><span style=""background-color: #ec547b;""><span style=""background-color: #fadbd9;"">[Column6]</span></span></span></span></td>
			</tr>
			<tr>
				<td style=""border-bottom: #ffffff 1px solid; text-align: center; padding-bottom: 5px; background-color: #ec547b; border-top-color: #ffffff; padding-left: 15px; padding-right: 15px; border-left-color: #ffffff; border-right: #ffffff 1px solid; padding-top: 5px;"">
					<span style=""font-family: 宋体, tahoma, arial, sans-serif;""><strong><span style=""font-weight: bold;""><span style=""font-family: 宋体, tahoma, arial, sans-serif;""><span style=""font-weight: bold;""><span style=""color: #ffffff;"">温馨提示</span></span></span></span></strong></span></td>
				<td colspan=""3"" style=""border-bottom: #ffffff 1px solid; text-align: left; padding-bottom: 5px; background-color: #ec547b; border-top-color: #ffffff; padding-left: 15px; padding-right: 15px; border-left-color: #ffffff; border-right: #ffffff 1px solid; padding-top: 5px;"">
					<span style=""font-family: 宋体, tahoma, arial, sans-serif;""><strong><span style=""color: #ffffff;"">[Column7]</span></strong></span></td>
			</tr>
			<tr>
			</tr>
		</tbody>
	</table>
</div>
<p>
	<!--商品描述--></p>
<div style=""text-align: left; width: 685px;"">
	&nbsp;</div>
<div style=""text-align: left; width: 685px;"">
	&nbsp;</div>
<div style=""text-align: left; width: 685px;"">
	<img src=""/fileRepository/product/others/Qlcvf-PJTheOrcFUTpV1kg.jpg"" /></div>
<div style=""text-align: left; width: 685px;"">
	&nbsp;</div>
<div style=""text-align: left; font-weight: bold;"">
	&nbsp;</div>
<div style=""text-align: left; font-weight: bold;"">
	&nbsp;</div>
<div style=""text-align: left; font-weight: bold;"">
	产品说明：</div>
<div style=""text-align: left; font-weight: bold;"">
	&nbsp;</div>
<div>
	[Column8]</div>
<div>
	&nbsp;&nbsp;</div>
<div>
	&nbsp;</div>
<div style=""text-align: left; font-weight: bold;"">
	&nbsp;</div>
<div style=""text-align: left; font-weight: bold;"">
	产品成分：</div>
<div>
	[Column9]</div>
<p>
	&nbsp;</p>
<div style=""text-align: left; font-weight: bold;"">
	产品详细介绍：</div>
<div>
	<p>
		[Column10]<br />
		&nbsp;</p>
	<p>
		
</div>
<div style=""text-align: left; font-weight: bold;"">
	产品使用方法：</div>
<p style=""text-align: justify; line-height: 16pt; margin-top: 0pt; margin-bottom: 0pt;"">
	&nbsp;</p>
<p>
	<span style=""font-size: 10.5pt;""><span style=""font-family: 宋体;"">[Column11]</span></p>
<p>
	<span style=""font-size: 10.5pt;""><span style=""font-family: 宋体;"">[Column12]</span></span></p>
<p>
	&nbsp;</p>
<p>
	<span style=""font-size: 10.5pt;""><span style=""font-family: 宋体;"">[Column13]</span></p>	
""";


            return template;
        }

    }

}
