﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenerateCodes
{
    class Config
    {
        public static string excelpath { get; set; }
        public static string outpath { get; set; }

        static Config()
        {
            excelpath = ConfigurationManager.AppSettings["excelpath"];
            outpath = ConfigurationManager.AppSettings["outpath"];
        }
    }
}
